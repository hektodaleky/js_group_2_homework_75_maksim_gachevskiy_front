import React, {Component} from "react";
import {Button} from "react-bootstrap";
import {connect} from "react-redux";
import {fetchMyData} from "../../store/actions";
class MainMenu extends Component {
    state = {
        inputTextVal: '',
        dropDownValue: 'encode',
        password: ''
    };
    changeText = (event) => {
        this.setState({inputTextVal: event.target.value})
    };


    changeDropDownValue = (event) => {
        this.setState({dropDownValue: event.target.value})
    };
    changePassword = (event) => {
        this.setState({password: event.target.value})
    };

    render() {

        return (<div className="container">
            <textarea className="form-control"
                      rows="8" id="comment"
                      value={this.state.inputTextVal}
                      onChange={this.changeText}></textarea>
            <Button onClick={() => this.props.onFetchData(this.state)}
                    bsStyle="primary">Translate</Button>
            <select value={this.state.dropDownValue} onChange={this.changeDropDownValue}>
                <option value="encode">Encode</option>
                <option value="decode">Decode</option>

            </select>
            <input placeholder="password" value={this.state.password} onChange={this.changePassword}/>
            <textarea className="form-control" rows="8" id="comment" value={this.props.responseText}></textarea>


        </div>)
    }
}
;

const mapStateToProps = state => {
    return {
        responseText: state.mainReducer.text
    };
};


const mapDispatchToProps = dispatch => {
    return {
        onFetchData: (coder, title) => dispatch(fetchMyData(coder, title))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);