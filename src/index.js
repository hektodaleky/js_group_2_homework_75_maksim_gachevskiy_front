import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunkMiddleware from "redux-thunk";

import reducer from "./store/reducer";
import {Provider} from "react-redux";
import {BrowserRouter} from "react-router-dom";
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const rootReducer = combineReducers({
    mainReducer: reducer
});

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunkMiddleware)));

const app = <Provider store={store}>
    <BrowserRouter>
        <App/>
    </BrowserRouter>
</Provider>
ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
