import {ERROR, FETCH, SUCCESS} from "./actions";
const initialState = {
    loading: false,
    error: false,
    text: ''
};

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case FETCH: {
            return {...state, loading: true, error: false}
        }
        case SUCCESS: {
            return {...state, loading: false, error: false, text: action.text}
        }
        case ERROR: {
            return {...state, loading: false, error: true}
        }

        default:
            return state;
    }
};
export default reducer;