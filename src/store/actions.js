import axios from "../axios";
export const FETCH = 'FETCH';
export const SUCCESS = 'SUCCESS';
export const ERROR = 'ERROR';


export const getFetch = () => {
    return {type: FETCH}
};
export const fetchSuccess = (text) => {
    return {type: SUCCESS, text}
};
export const fetchError = () => {
    return {type: ERROR}
};

export const fetchMyData = (requestPram) => {
    const link=`/${requestPram.dropDownValue}/${requestPram.inputTextVal}`;
    return dispatch => {


         axios.post(link,{pass:requestPram.password}).then(
            response => {
                dispatch(fetchSuccess(response.data));

            }
        ).catch((log) => {

        });

    }
};